#include "test.h"


void test_alloc_and_free() {
    printf("Test 1:\n");
    block_size capacity = {REGION_MIN_SIZE};
    void *heap = heap_init(capacity_from_size(capacity).bytes);
    if (!heap) {
        fprintf(stderr, "\nFailed at test 1");
        return;
    }
    debug_heap(stdout, heap);

    void *block = _malloc(256);
    if (!block) {
        fprintf(stderr, "\nFailed at test 1");
        return;
    }
    debug_heap(stdout, heap);
    _free(block);
    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    printf("\nPassed");

}

void test_less_than_min_size() {
    printf("\nTest 2:\n");
    block_size capacity = {REGION_MIN_SIZE};
    void *heap = heap_init(capacity_from_size(capacity).bytes);
    if (!heap) {
        fprintf(stderr, "\nFailed at test 2");
        return;
    }
    void *block = _malloc(12);
    if (!block) {
        fprintf(stderr, "\nFailed at test 2");
        return;
    }

    debug_heap(stdout, heap);
    _free(block);
    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    printf("\nPassed");


}

void test_free_blocks() {
    printf("\nTest 3:\n");
    block_size capacity = {REGION_MIN_SIZE};
    void *heap = heap_init(capacity_from_size(capacity).bytes);
    if (!heap) {
        fprintf(stderr, "\nFailed at test 3");
        return;
    }
    void *block1 = _malloc(51);
    void *block2 = _malloc(23);
    void *block3 = _malloc(890);
    void *block4 = _malloc(324);

    if (!block1 || !block2 || !block3 || !block4) {
        fprintf(stderr, "\nFailed at test 3");
        return;
    }
    debug_heap(stdout, heap);
    _free(block1);
    _free(block3);
    debug_heap(stdout, heap);
    _free(block2);
    _free(block4);
    munmap(heap, REGION_MIN_SIZE);
    printf("\nPassed");

}

void test_over_expansion() {
    printf("\nTest 4:\n");
    void *heap = heap_init(1);
    if (!heap) {
        fprintf(stderr, "\nFailed");
        return;
    }
    debug_heap(stdout, heap);

    void *block1 = _malloc(1);
    void *block2 = _malloc(1);
    void *block3 = _malloc(1);
    if (!block1 || !block2 || !block3) {
        fprintf(stderr, "\nFailed at test 4");
        return;
    }

    debug_heap(stdout, heap);


    void *block = _malloc(REGION_MIN_SIZE);
    if (!block) {
        fprintf(stderr, "\nFailed at test 4");
        return;
    }
    debug_heap(stdout, heap);

    _free(block);
    _free(block1);
    _free(block2);
    _free(block3);
    debug_heap(stdout, heap);


    munmap(heap, 2 * REGION_MIN_SIZE);
    printf("\nPassed");

}

void test_memory_move() {
    printf("\nTest 5:\n");
    block_size capacity = {REGION_MIN_SIZE};
    void *heap = heap_init(capacity_from_size(capacity).bytes);
    if (!heap) {
        fprintf(stderr, "\nFailed at test 5");
        return;
    }

    debug_heap(stdout, heap);

    (void) mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                MAP_PRIVATE, -1, 0);

    void *block = _malloc(REGION_MIN_SIZE * 2);
    if (!block) {
        fprintf(stderr, "\nFailed at test 5");
        return;
    }

    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    printf("\nPassed");

}


void start_tests() {
    test_alloc_and_free();
    printf("\n________________________________________");
    test_less_than_min_size();
    printf("\n________________________________________");
    test_free_blocks();
    printf("\n________________________________________");
    test_over_expansion();
    printf("\n________________________________________");
    test_memory_move();
}